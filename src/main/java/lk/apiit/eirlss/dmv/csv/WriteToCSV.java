package lk.apiit.eirlss.dmv.csv;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import lk.apiit.eirlss.dmv.models.Reported;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class WriteToCSV {


    public static void writeReportedToCSV(PrintWriter writer, List<Reported> list) {
        String[] CSV_HEADER = {
                "id",
                "lNumber",
                "reportedDate",
                "firstName",
                "lastName",
                "nic",
                "offence"
        };
        StatefulBeanToCsv<Reported> beanToCsv = null;
        try (
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ){
            csvWriter.writeNext(CSV_HEADER);

            // write List of Objects
            ColumnPositionMappingStrategy<Reported> mappingStrategy =
                    new ColumnPositionMappingStrategy<>();

            mappingStrategy.setType(Reported.class);
            mappingStrategy.setColumnMapping(CSV_HEADER);

            beanToCsv = new StatefulBeanToCsvBuilder<Reported>(writer)
                    .withMappingStrategy(mappingStrategy)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            beanToCsv.write(list);

            System.out.println("Write CSV using BeanToCsv successfully!");
        }catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }
    }

}
