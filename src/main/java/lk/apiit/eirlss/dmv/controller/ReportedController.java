package lk.apiit.eirlss.dmv.controller;

import lk.apiit.eirlss.dmv.csv.WriteToCSV;
import lk.apiit.eirlss.dmv.models.Reported;
import lk.apiit.eirlss.dmv.service.ReportedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RestController
public class ReportedController {

    private ReportedService service;


    @Autowired
    public ReportedController(ReportedService service) throws Exception {
        this.service = service;

        List<Reported> raw = service.getAllReported();
        if(raw.size()==0){
            Reported reported = new Reported();
            reported.setFirstName("Sam");
            reported.setLastName("Udalagama");
            reported.setContactNumber("0713031690");
            reported.setLNumber("12345678");
            reported.setReportedDate(new Date());
            reported.setNic("850362837V");
            reported.setOffence("lost");
            service.initalData(reported);
        }

    }

    @GetMapping("/getReported")
    public void  getReported(HttpServletResponse response) throws Exception{
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; file=reported.csv");
        List<Reported> reportedList = service.getAllReported();

        WriteToCSV.writeReportedToCSV(response.getWriter(),reportedList);
    }
}
