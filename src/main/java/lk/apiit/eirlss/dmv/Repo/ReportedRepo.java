package lk.apiit.eirlss.dmv.Repo;

import lk.apiit.eirlss.dmv.models.Reported;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportedRepo extends CrudRepository<Reported, String> {
}
