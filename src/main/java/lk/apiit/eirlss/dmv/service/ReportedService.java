package lk.apiit.eirlss.dmv.service;

import lk.apiit.eirlss.dmv.Repo.ReportedRepo;
import lk.apiit.eirlss.dmv.models.Reported;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ReportedService {

    @Autowired
    private ReportedRepo repo;

    public List<Reported> getAllReported() throws Exception{
        try{
            List<Reported> list = (List<Reported>) repo.findAll();
            return list;

        }catch (Exception e){
            throw new Exception("ERROR GETTING DATA", e);
        }
    }

    public boolean initalData(Reported reported){
        repo.save(reported);
        return true;
    }


}
