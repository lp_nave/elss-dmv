package lk.apiit.eirlss.dmv.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Reported")
@NoArgsConstructor
@AllArgsConstructor
public class Reported {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "reportID")
    private String id;

    @Column(name = "licenseNumber")
    private String lNumber;

    @Column(name = "reportedDate")
    @Temporal(TemporalType.DATE)
    private Date reportedDate;

    @Column( name="firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "contactNumber")
    private String contactNumber;

    @Column(name = "nic")
    private String nic;

    @Column(name = "offence")
    private String offence;

}
